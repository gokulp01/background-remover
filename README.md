# Background removal tool
## Dependencies:
- Python 3
- Open CV

To run the script, make sure that there are 2 images in the same directory
named:
- `background_image.jpg` for the background image
- `image.jpg` for the image that the script is used for

Clone the repository:  `git clone https://gitlab.com/gokulp01/background-remover.git`

Then all you have to do is run the executable named background_remover

### Input:

![Input](image.jpg)

### Output:

![Input](output.png)

#### Author:
Gokul P

To know about the working of the code and the code itself:
https://www.roboslog.in/background-subtraction-using-open-cv-on-videos/
